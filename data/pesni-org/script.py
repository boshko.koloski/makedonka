# -*- coding: utf-8 -*-
"""
Created on Mon Oct 19 13:56:27 2020

@author: Bosec
"""
import requests
from tqdm import tqdm
from bs4 import BeautifulSoup
import pandas

data_text = []
for i in tqdm(range(1326)):
    URL = 'https://pesna.org/song.php?id='+str(i)
    page = requests.get(URL)
    soup = BeautifulSoup(page.content, 'html.parser')    
    #head_line = soup.find("title").getText()
    print()
    #head_line = head_line.split("|")[0].strip() 
    #print(head_line)
    mydivs = soup.find_all("div", class_="row lyrics")  
    for x in mydivs:
        tmp = x.find('p').getText(" ") # replace("</br>","\n")    
        print(tmp)
        data_text.append(tmp)

print(data_text)
        
df = pandas.DataFrame({'text':data_text})
df.to_csv("dataset.csv", index=False)
