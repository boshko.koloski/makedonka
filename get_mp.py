
from multiprocessing import Pool, TimeoutError
import pandas as pd
import json
import requests
from bs4 import BeautifulSoup

def fil(lines):
    from tqdm import tqdm
    outs = []
    for line in tqdm(lines):
        line = json.loads(line)
        URL = line['url']
        page = requests.get(URL)
        soup = BeautifulSoup(page.content, "html.parser")
        try:
            xs = soup.find_all('p')
            texts = " ".join([p.text for p in xs])
        except:
            texts = ""
        try:
            ys = soup.find('div', {"class":'bottom-tags'}).find_all('a')
            ys = ";".join([y.text for y in ys])
        except:
            ys = ""
        line['text'] = texts
        line['tags'] = ys
        outs.append(line)
    return outs
import numpy as np
if __name__ == '__main__':
    f = open('data/it_mk.json','r')
    lines = [l for l in f]#[:8]
    with Pool(processes=8) as pool:
        res = pool.map(fil, [l for l in np.array_split(lines, 8)])
    outs = []
    import pickle
    with open('outs.pkl','wb') as fp:
        pickle.dump(outs, fp)
    for r in res:
        outs = outs + r

    df = pd.DataFrame(outs)
    df.to_csv('outs.csv', index = None)